<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // dd('hi');

         $students = Student::orderBy('created_at', 'desc')->simplePaginate(15);
        //  dd($students);
          return view('admin.content.student.index', compact('students'));

        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('admin.content.student.create');

       
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $inputs = $request->all();
        $student = Student::create($inputs);
        return redirect()->route('admin.content.student.index')->with('swal-success', 'دانشجوی جدبد  جدید  با موفقیت اضافه شد');

        
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        return view('admin.content.student.edit', compact('student'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Student $student)
    {

        $inputs = $request->all();
        
        

        $student->update($inputs);
        return redirect()->route('admin.content.student.index')->with('swal-success', 'دانشجو مورد نظر با موفقیت ویرایش شد');
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {


        $result = $student->delete();
        return redirect()->route('admin.content.student.index')->with('swal-success', 'دانشجو با موفقیت حذف شد');
        
        //
    }
}
