<?php

namespace App\Http\Controllers;

use App\Models\Available_course;
use App\Http\Requests\StoreAvailable_courseRequest;
use App\Http\Requests\UpdateAvailable_courseRequest;
use App\Models\Master;
use App\Models\Student;
use App\Models\Course;
use Illuminate\Http\Request;


class AvailableCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $available_courses =Available_course::find(1);
        // dd( $available_courses->master->last_name);
        $available_courses = Available_course::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.content.available_course.index', compact('available_courses'));
        // dd($available_courses->all());
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $courses = Course::all();
        $masters = Master::all();
        

        return view('admin.content.available_course.create', compact('courses','masters'));

        
        //s
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $available_course = Available_course::create($inputs);
        return redirect()->route('admin.content.available_course.index')->with('swal-success', 'درس  جدید  با موفقیت ارائه شد');
    }

    /**
     * Display the specified resource.
     */
    public function show(Available_course $available_course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Available_course $available_course)
    {
        
        $courses = Course::all();
        $masters = Master::all();
        return view('admin.content.available_course.edit', compact('available_course', 'courses','masters'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Available_course $available_course)
    {

        $inputs = $request->all();
        
        

        $available_course->update($inputs);
        return redirect()->route('admin.content.available_course.index')->with('swal-success', 'درس ارائه شده با موفقیت ویرایش شد');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Available_course $available_course)
    {
        $result = $available_course->delete();
        return redirect()->route('admin.content.available_course.index')->with('swal-success', 'درس ارائه شده  شما با موفقیت حذف شد');
        //
    }
}
