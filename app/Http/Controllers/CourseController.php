<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Master;
use App\Models\Student;
use App\Models\Available_course;
use App\Http\Requests\StoreCourseRequest;
use App\Http\Requests\UpdateCourseRequest;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $courses = Course::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.content.course.index', compact('courses'));

        

        return view('admin.index');
        // $course=Course::find(1);
        // dd($course->masters);

        //  $master=Master::find(1);
        //  dd($master->courses);


        // $student=Student::find(1);
        // dd($student->availableCourses);

        
        // $available_course=Available_course::find(1);
        // dd($available_course->students);





        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('admin.content.course.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

       
        $inputs = $request->all();
        $course = Course::create($inputs);
        return redirect()->route('admin.content.course.index')->with('swal-success', 'درس مورد نظر با موفقیت اضافه شد');
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Course $course)
    {

        return view('admin.content.course.edit', compact('course'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Course $course)
    {
        $inputs = $request->all();
        
        

        $course->update($inputs);
        return redirect()->route('admin.content.course.index')->with('swal-success', 'درس مورد نظر با موفقیت ویرایش شد');
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Course $course)
    {
        $result = $course->delete();
        return redirect()->route('admin.content.course.index')->with('swal-success', 'درس مورد نظر با موفقیت حذف شد');
        //
    }
}
