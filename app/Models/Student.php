<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Available_course;
use App\Models\Student;

class Student extends Model
{
    use HasFactory;
    // public function selected_course(){
    //     return $this->hasmany(Selected_course::class);
    // }

    public function availableCourses(){
        return $this->belongsToMany(Available_course::class,'selected_courses','available_id','student_id');
    }

    protected $fillable = ['first_name', 'last_name'];
  
}
